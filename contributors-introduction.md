# Collaborators Welcome
#### Using GitLab

Being on GitLab means that you can participate easily in this project, raise issues, and contribute to the project by making a copy and submitting a pull-request with your changes.

It's ideal for collaborative work, where visitors may simply copy (clone\fork) to work with, independently, or in parallel.

[**GitLab Basics**](https://docs.gitlab.com/ee/gitlab-basics/)

To contribute, you copy the project, by forking it, make changes on your copy, and then submit changes to be merged into the master project.

> [Fork a project](https://docs.gitlab.com/ee/gitlab-basics/fork-project.html), to duplicate projects so they can be worked on in parallel.
> [Add a file](https://docs.gitlab.com/ee/gitlab-basics/add-file.html), to add new files to a project’s repository.
> [Create an issue](https://docs.gitlab.com/ee/gitlab-basics/add-file.html), to start collaborating within a project.
> [Create a merge request](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html), to request changes made in a branch be merged into a project’s repository.

##### Using Git

Git is a collaborative file management program that keeps track of all changes in a project, and enables programmers to work on software together. 

Gitlab is a web interface that makes it much simpler, since you can start using it as a web-app, and makes sharing and connecting with others, much easier.

[Git Basics - Clone a Repository](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-a-repository)

If you want to copy contents of one of these repositories to work with locally.

##### Fun with Submodules

Both the Allan Mulhern and Jordan Peterson directories, are individual projects, here on gitlab. 

* [jungian-dark-web/jordan-b-peterson](https://gitlab.com/jungian-dark-web/jordan-b-peterson)
* [jungian-dark-web/alan-mulhern](https://gitlab.com/jungian-dark-web/alan-mulhern)

When contributing, be sure to work from there, rather than the submodule linked in this repo.