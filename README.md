# The Jungian Dark Web

Recently, I found the podcast of Jordan Peterson, which set me back on a quest of self understanding through Jungian psychology. 

I haven't studied Carl Jung's work in as near the depth as those who's work is featured here. However, I did read [*Memories, Dreams, Reflections*](https://monoskop.org/images/1/10/Jaffe_Aniela_Memories_dreams_reflections_1989.pdf) as a young man, and spent a good deal of time with [*Man and His Symbols*](https://monoskop.org/images/9/97/Von_Franz_Luise_Marie_Jung_Gustav_Carl_Man_and_His_Symbols_1988.pdf), in recent years. 

To begin really exploring his work, in depth, contemporary sources are most accessible. 

#### To Become a Comparative Overview of Jungian thought

My intention is to build a high level map of Jungian thought, in nested lists of links. I've begun focused on Peterson's work, since he presents Jung in relation to western thought, and explores much territory from a Jungian perspective. 

Working on this project serves both my educational, and theraputic, processes. It's medatative, and supports having an intimate relationship with complex subjects.

It's useful for personal reference, and hopefully will help others more easily survey the territory surrounding Jung. Another hope is that with the creation of this resource, I, a mere mortal, might develop the ability to discuss intelligently, without losing track of essential concepts or references.

##### TOC

* [Carl Gustav Jung](#carl-gustav-jung)
  * [Dr. Jordan Peterson](#dr-jordan-peterson)
  * [Alan Mulhern](#alan-mulhern)
  * [Jungianthology](#jungianthology)
* [On the Intellectual Dark Web](#on-the-intellectual-dark-web)

##### Dark Light: Cosmic Web

![](http://i.imgur.com/rt5IkfD.png)\
[Beauty & Unease Coexist in Carol Prusa’s “Dark Light”](https://www.bocamag.com/beauty-unease-coexist-in-carol-prusas-dark-light/)

## Carl Gustav Jung 

[Carl Jung - Curated List](carl-gustav-jung.md)

This is the least developed resource, thus far. For large topics, it's often easier for me to work around the edges, like working on a puzzle.

### Dr. Jordan B Peterson

* [Jordan Peterson](jordan-peterson/)
  * [Blog](jordan-peterson/blog.md)
  * [Youtube](jordan-peterson/youtube.md)
  * [Psych 230h - Personality and its Transformations - Introduction and Overview](jordan-peterson/personality-and-its-transformation/)
  * [Psychology 434 – Maps of Meaning](jordan-peterson/personality-and-its-transformation/)
  * [Biblical Lectures - Genesis](jordan-peterson/biblical-lectures.md)

I will be going over Psych 230h over the coming months, as its presents an overview and comparative of western throught, showing where Jung fits in relation. That will help digest my own unformed positions, and eventually I'll explore that theme more on my own.

Really, with regards to his content, I'm most excited about digging into Maps of Meaning, and hopefully someday he'll continue the Biblical lectures, on to Exodus.

### Alan Mulhern

Alan Mulhern details the process of individuation through the lense of psychotheraputic practice in the first season of his podcast. These episodes are short and information dense, which is where working with the material, here, really comes in handy.
  
[**Alan Mulhern - Index**](alan-mulhern/)
  * [Healing of Emotional Wounds](alan-mulhern/healing-emotional-wounds/) - Psychotherapy: Jungian Approach to Healing. 
    * [Introduction: Transcript](alan-mulhern/healing-emotional-wounds/Introduction.md)
  * [The Quest](alan-mulhern/quest-series.md) - An exploration of great visionaries who have shaped our history and contemporary world. With special focus on the evolving crises of the 21st century.

### Jungianthology 

Jungianthology offers the opening lecture from a variety of its courses, which are available for purchase, complete, from their website. It presents an array of Jungians, of various psychological traditions and backgrounds, each offering their unique perspective in the living tradition that has forwarded the work of Carl Gustav Jung. Not sure if I could afford to purchas all the lectures I'm interested in.. however, even the opening lecture for each course explains what will be covered, providing plenty of leads. 

[**Jung Chicago - Jungianthology Podcast - Index**](jung-chicago/)

#### On The Intellectual Dark Web

I was introduced to the idea of the Intellectual Dark Web through Dr. Peterson, though I was already familiar with Joe Rogan, Sam Harris, and others in the same class of early podcasters. 

Critics often seem to focus on an offensive idea they don't like, which seems to me but a sliver, within a wealth of information.

* [Intellectual Dark Web (IDW)](https://intellectualdarkweb.site/)

Jung has also faced much criticism, and it's difficult to appreciate his work without consuming a great deal of it, along with related material from disparate sources. 

It will be easier to properly criticize these thinkers, having a high level map of their ideas to work with.


---

[![](http://jungcurrents.com/wp-content/uploads/2014/02/jung-enemy-loved-latest.jpg)](http://jungcurrents.com/cg-jung-alms-kindness-enemy-loved)