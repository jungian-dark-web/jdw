# Carl Gustav Jung - Curated List

* [The Jung Page](http://www.cgjungpage.org/)
* [r/Jung](https://www.reddit.com/r/Jung/)
* [Carl-Jung.net](http://www.carl-jung.net/)
* [Mythos and Logos - Jung](http://mythosandlogos.com/Jung.html)
* [Gnosis](http://www.gnosis.org/gnostic-jung/Jung-and-Gnosis.html)

### Books
* [Searchable Jung Concordance](http://aras.org/concordance)
* [AION full book free, multiple formats](https://archive.org/details/collectedworksof92cgju)
* [Jung On Monoskop](https://monoskop.org/index.php?search=carl+jung) - wiki for the arts, media and humanities.

### Typology
* [Michael Pierce presents: supplementary materials for the typology enthusiast.](http://subjectobjectmichaelpierce.blogspot.com/) - Subject Object
  * [Readable "Psychological Types"](http://subjectobjectmichaelpierce.blogspot.com/p/blog-page.html) ([Psychological Types](https://en.wikipedia.org/wiki/Psychological_Types) on wikipedia)
* [Jung's Psychology of Types, MBTI](http://mbtitoday.org/carl-jung-psychological-type/)

### Red Book
* [Jung's Red Book lectures](http://www.gnosis.org/redbook/index.html)
* [Red Book Pictoral Guide](https://aras.org/sites/default/files/docs/00033Sherry.pdf)
* [All illustrations from Red Book](https://issuu.com/redbooklove/docs/all_of_the_images_from_the_red_book_by_carl_jung)

### Continuation of Jungs Work

* [Collection of Joseph Campbell Lectures](http://www.openculture.com/2015/08/48-hours-of-joseph-campbell-lectures-free-online.html)
* [Jungianthology Podcast - Jung Chicago](https://jungchicago.org/blog/)
* [Dr. Robert Moore talks about Jung](https://www.robertmoore-phd.com/index.cfm?fuseaction=category.display&category_ID=23)
* [List of Jung Societies. Use link #4](http://www.cgjungpage.org/links)
