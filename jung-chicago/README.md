# Jungianthology

####  Gather Up Your Brokenness: Love, Imperfection, & Human Ideals (Part 2)

In celebration of our Holiday Giving Drive, we are unlocking a full seminar by Polly Young-Eisendrath, “Gather Up Your Brokenness: Love, Imperfection, & Human Ideals”. You can be a part…

[Read More](https://jungchicago.org/blog/gather-up-your-brokenness-love-imperfection-human-ideals-part-2/)

####  Gather Up Your Brokenness: Love, Imperfection, & Human Ideals (Part 1)

In celebration of our Holiday Giving Drive, we are unlocking a full seminar by Polly Young-Eisendrath, “Gather Up Your Brokenness: Love, Imperfection, & Human Ideals”. You can be a part…

[Read More](https://jungchicago.org/blog/gather-up-your-brokenness-love-imperfection-human-ideals-part-1/)

####  Why Become a Jungian Psychoanalyst: An Interview with Adina Davidson & Dan Ross

Boris Matthews, PhD, LCSW, NCPsyA, the Director of Training at the C.G. Jung Institute of Chicago, interviews two Jungian Psychoanalysts and recent graduates of our Analyst Training Program (ATP), Adina…

[Read More](https://jungchicago.org/blog/why-become-a-jungian-psychoanalyst-an-interview-with-adina-davidson-dan-ross/)

####  Thomas Moore on Aging and the Soul

Thomas Moore will be visiting us in October to lead a two-day seminar (registration is open now). As an introduction to Moore’s perspective and voice, we are sharing the first…

[Read More](https://jungchicago.org/blog/thomas-moore-on-aging-and-the-soul/)

####  Interview with Marcus West

Marcus West is a Training and Supervising Analyst of the Society of Analytical Psychology and is Co-Editor-in-Chief of the Journal of Analytical Psychology. He is author of a number of…

[Read More](https://jungchicago.org/blog/interview-with-marcus-west/)

####  Mythology and Clinical Practice

with Nathan Schwartz-Salant, PhD Dr. Schwartz-Salant explores the importance of Mythology for understanding and containing psychic life within the analytic process. The myths of Pan, Dionysus, Gilgamesh, and Egyptian images…

[Read More](https://jungchicago.org/blog/mythology-and-clinical-practice/)

####  Facing the Gods: Archetypal Patterns of Existence

Preparing to Meet the Gods: The Soul Turned Inward with John Van Eenwyk, PhD Experiencing the archetypes as personified gods and goddesses active in our lives reveals the great powers…

[Read More](https://jungchicago.org/blog/facing-the-gods-archetypal-patterns-of-existence/)

####  Breaking the Code of the Archetypal Self: An Introductory Overview of the Research Discoveries Leading to Neo-Jungian Structural Psychoanalysis

with Robert Moore, PhD This lecture is the first part of the series Structural Psychoanalysis and Integrative Psychotherapy: Introduction to a Neo-Jungian Paradigm, which contains the following lectures: Lecture 1 – Breaking the Code of…

[Read More](https://jungchicago.org/blog/breaking-the-code-of-the-archetypal-self/)

####  Jung’s Concept of the Animus

with Lucille Klein, MA, NCPsyA With the current debate over the nature and content of gender, Jung’s concepts of the anima/animus are being re-examined and, in some cases, reformulated or…

[Read More](https://jungchicago.org/blog/jungs-concept-of-the-animus/)

####  The Four Couples Within: The Structure of the Self and the Dynamics of Relationship

with Robert Moore, PhD This episode is part one of the series The Four Couples Within: The Structure of the Self and the Dynamics of Relationship. It was recorded in…

[Read More](https://jungchicago.org/blog/the-four-couples-within/)

####  The Structure and Dynamics of the Psyche

with August Cwik, PsyD This episode is the first half of Structure and Dynamics of the Psyche: The World According to C.G. Jung. It was recorded in 1992. Cwik introduces the…

[Read More](https://jungchicago.org/blog/the-structure-and-dynamics-of-the-psyche/)

####  Understanding and Healing Addictions: A Jungian Contribution

with John Giannini, MDiv, LCPC, NCPsyA This episode is part one of the series Understanding and Healing Addictions: A Jungian Contribution. It was recorded in 1990. This course offers a…

[Read More](https://jungchicago.org/blog/understanding-and-healing-addictions-a-jungian-contribution/)

####  Mythology and Psychology: A Jungian Perspective

with Robert Moore, PhD This episode is part one of the series Myth and Psyche: An Introduction to Jungian Perspectives on Human Mythology. It was recorded in 1992. According to…

[Read More](https://jungchicago.org/blog/mythology-and-psychology-a-jungian-perspective/)

####  The Way of the Sly One: Gurdjieff, Ouspensky, & Jung

with Ken James, PhD This episode is the first part of the series The Way of the Sly One: The Psychology of Our Possible Evolution in the Writings of Gurdjieff, Ouspensky,…

[Read More](https://jungchicago.org/blog/the-way-of-the-sly-one-gurdjieff-ouspensky-jung/)

####  Bonus: The Fate of Depth Psychology in the New Millenium

with June Singer and other Analysts As we enter a new year, it seems right to share the recording of the program “The Fate of Depth Psychology in the New…

[Read More](https://jungchicago.org/blog/bonus-the-fate-of-depth-psychology-in-the-new-millenium/)

####  Women’s Mysteries: Sources of Creativity, Religion & Spirituality, & Solace

with Jean Shinoda Bolen, MD This episode is the first part of the series Women’s Mysteries: Sources of Creativity, Religion & Spirituality, & Solace. Jungian analyst and author Jean Shinoda…

[Read More](https://jungchicago.org/blog/womens-mysteries-sources-of-creativity-religion-spirituality-solace/)

####  A Psychological Approach to the Bible

with Murray Stein, PhD This lecture, “Origins: The Ego Once- and Twice-Born”, is part one of the series A Psychological Approach to the Bible I. It was recorded in 1989.…

[Read More](https://jungchicago.org/blog/a-psychological-approach-to-the-bible/)

####  Jungian Psychology & Kohut’s Self-Psychology

with Lionel Corbett, MD & Cathy Rives, MD This episode is the first session of the series Jungian Psychology & Kohut’s Self-Psychology. The psychoanalytic methods of self psychology as developed…

[Read More](https://jungchicago.org/blog/jungian-psychology-kohuts-self-psychology/)

####  Individuation, Adaptation, & Psychological Type (Rebroadcast)

with Boris Matthews, PhD, LCSW. We are rebroadcasting this episode because it inexplicably disappeared from our iTunes feed. The work of C.G. Jung offers thoughtful clinicians useful, practical insights into…

[Read More](https://jungchicago.org/blog/individuation-adaptation-psychological-type-rebroadcast/)

####  Christian Shamanism

with Thomas Patrick Lavin, PhD This episode is the first session of the series Christian Shamanism: Visions of Nikolas of Flue. A shaman is a person who has been forced by…

[Read More](https://jungchicago.org/blog/christian-shamanism/)

####  Jungian Psychology and Human Spirituality: Liberation from Tribalism in Religious Life

with Robert Moore, PhD This episode is part one of the series Jungian Psychology and Human Spirituality: Liberation from Tribalism in Religious Life. It was recorded in 1989. In this…

[Read More](https://jungchicago.org/blog/jungian-psychology-and-human-spirituality-liberation-from-tribalism-in-religious-life/)

####  The Archetypal Realities of Everyday Life

with Anthony Stevens, MD This episode is part one of the series The Archetypal Realities of Everyday Life. It was recorded in 1986. This seminar examines the ways in which…

[Read More](https://jungchicago.org/blog/the-archetypal-realities-of-everyday-life/)

####  Understanding the Meaning of Alchemy: Jung’s Metaphor for the Transformative Process

with Murray Stein, PhD This episode is part one of the series Understanding the Meaning of Alchemy. It was recorded in 1992. During the last thirty years of his life,…

[Read More](https://jungchicago.org/blog/understanding-the-meaning-of-alchemy-jungs-metaphor-for-the-transformative-process/)

####  Jung’s Commentary on the Spiritual Exercises of Ignatius of Loyola

with Thomas Patrick Lavin, PhD This episode is the first session of the four-part series Jung’s Commentary on the Spiritual Exercises of Ignatius of Loyola. Using as a focal point Jung’s…

[Read More](https://jungchicago.org/blog/jungs-commentary-on-the-spiritual-exercises-of-st-ignatius-of-loyola/)

####  Walking the Way of Individuation

with Ken James, PhD This episode is the first session of the four-part series The Path is the Goal: Walking the Way of Individuation. Jung called individuation the method by which…

[Read More](https://jungchicago.org/blog/walking-the-way-of-individuation/)

####  A New Model of Psychological Types

with John Beebe, MD This episode is the first hour of the seminar A New Model of Psychological Types. Jung’s theory of psychological types is an attempt to make comprehensible the…

[Read More](https://jungchicago.org/blog/a-new-model-of-psychological-types/)

####  The Psychology of Fairy Tales

with Lois Khan, PhD This episode is “Go I Know Not Whither, Bring Back I Know Not What”, part one of the series The Psychology of Fairy Tales. “Fairy tales…

[Read More](https://jungchicago.org/blog/go-i-know-not-whither-bring-back-i-know-not-what/)

####  Jungian Women: The First Generation

with June Singer, PhD This episode is part one of the series Jungians Speak About Jungian Women. Women’s contributions have been central to the development of Jung’s analytical psychology from…

[Read More](https://jungchicago.org/blog/jungian-women-the-first-generation/)

####  Transforming Depression Through Symbolic Death and New Life: Using the Creative Arts

with David Rosen, MD This episode is part one of the series Transforming Depression Through Symbolic Death and New Life: A Jungian Approach to Using the Creative Arts. While working…

[Read More](https://jungchicago.org/blog/transforming-depression-through-symbolic-death-and-new-life-using-the-creative-arts/)

####  Conversation with Arwind Vasavada

Arwind Vasavada (1912-1998) was born and raised in India. In the 1950’s, he traveled to Zurich to study at the Jung Institute and to work in analysis with C.G. Jung.…

[Read More](https://jungchicago.org/blog/conversation-with-arwind-vasavada/)

####  Mythologies of Journey & Pilgrimage

with Thomas Patrick Lavin, PhD This episode is part one of the series Myths to Grow By. In his later years, Joseph Campbell defined mythology as a system of energy-evoking…

[Read More](https://jungchicago.org/blog/mythologies-of-journey-pilgrimage/)

####  Terror, Evil, & Loss of the Self

with Brenda Donahue, RN, LCSW This episode is part one of the series Terror, Evil, and Loss of the Self. In this seminar, Brenda Donahue discusses how survivors of childhood deprivation…

[Read More](https://jungchicago.org/blog/terror-evil-loss-of-the-self/)

####  The Religious Functions of the Psyche

with Lionel Corbett, MD This episode is part one of the series The Religious Functions of the Psyche. In this seminar, Lionel Corbett reviews developments in self psychology from the…

[Read More](https://jungchicago.org/blog/the-religious-functions-of-the-psyche/)

####  Consciousness: Theory of Ego and Ego Complex

with Murray Stein, PhD This episode is part one of the series The Jungian Psyche: A Deeper Look at Analytical Psychology. The course, recorded in 1991, offers a careful exploration…

[Read More](https://jungchicago.org/blog/15-stein-consciousness-theory-of-ego/)

####  Jung & Spirituality

with Warren Sibilla, Jr, PhD. Using examples from Zen Buddhism, Warren Sibilla discusses Jung’s idea that the subjective and objective have a complementary relationship, and that this relationship is necessary…

[Read More](https://jungchicago.org/blog/jung-spirituality/)

####  The Father’s Anima as a Clinical and Symbolic Problem

with John Beebe, MD. In this lecture, Dr. Beebe explores a neglected area in analytical psychology, the influence of the father’s unconscious upon the later development of the son. Jung’s…

[Read More](https://jungchicago.org/blog/the-fathers-anima-as-a-clinical-and-symbolic-problem/)

####  Individuation, Adaptation, & Psychological Type

with Boris Matthews, PhD, LCSW. The work of C.G. Jung offers thoughtful clinicians useful, practical insights into the emotional lives of clients. Yet much of his work remains unknown to…

[Read More](https://jungchicago.org/blog/individuation-adaptation-psychological-type/)

####  Same-Sex Love: Archetypal Reflections

with Karin Lofthus Carrington, MA, MFT. Caroline Stevens, Jungian analyst and wise woman of our Jungian community, introduces Karin Carrington, psychotherapist, author, and teacher who shares her reflections and understandings…

[Read More](https://jungchicago.org/blog/same-sex-love-archetypal-reflections/)

####  The Pilgrimage Home

Rediscovering the Wisdom of the Earth with China Galland. One of the most important features of a pilgrimage is its intimate association with nature through the kaleidoscope of changing weather and…

[Read More](https://jungchicago.org/blog/the-pilgrimage-home/)

####  Crones Don’t Whine: Concentrated Wisdom for Juicy Women and Exceptional Men

with Jean Shinoda Bolen, M.D. To be a crone is not a matter of age or appearance. Becoming a “crone” is a crowning inner achievement. “Crones Don’t Whine” is the…

[Read More](https://jungchicago.org/blog/crones-dont-whine/)

####  Befriending the Beast

with Anita Greene, Ph.D. Anita Greene, Ph.D. is a Jungian analyst in private practice in Amherst Massachusetts, and a teacher at the C.G. Jung Institute in Boston. She is also…

[Read More](https://jungchicago.org/blog/befriending-the-beast/)

####  Boundaries of the Soul: The Practice of Jung’s Psychotherapy

with June Singer, PhD In this talk June Singer gives an overview of Jungian Psychology, describes how the Jungian relationship to the unconscious differs from other forms of depth psychology,…

[Read More](https://jungchicago.org/blog/boundaries-of-the-soul/)

####  Early Trauma and Dreams: Archetypal Defenses of the Personal Spirit

with Donald Kalsched, Ph.D. Donald Kalsched, PhD is a Clinical Psychologist and Jungian Psychoanalyst in private practice in Albuquerque, New Mexico. He is a senior training analyst with the Inter-Regional Society…

[Read More](https://jungchicago.org/blog/early-trauma-and-dreams/)

####  An Introduction to Jung’s Life and Work

Murray Stein presents an historical overview of Jung’s life and work, detailing his relationship with Freud, and discussing reasons for Jung’s increasing popularity and relevance for contemporary society. This seminar…

[Read More](https://jungchicago.org/blog/an-introduction-to-jungs-life-and-work/)

####  Mother Earth Body Self: Therapeutic Process as Return and (Re-) Emergence

with Sylvia Brinton Perera, MA Just as earth is source, support, and home to humankind, so the mother’s body is source, support, and home of each infant. When the individual’s…

[Read More](https://jungchicago.org/blog/mother-earth-body-self/)

####  Individuation in Marriage Through Wounding and Healing

Marriage, life’s greatest intimacy, paradoxically delivers both wounding and healing and challenges to the full our capacities for self-acceptance and self-giving. In this lecture, Dr. Stein examines the mysteries and…

[Read More](https://jungchicago.org/blog/individuation-in-marriage-through-wounding-and-healing/)

####  Jungian Views on Aging

with Lionel Corbett, MD This recording is the final segment of a series of lectures given by Lionel Corbett and includes a lengthy question and answer period. Themes include: The…

[Read More](https://jungchicago.org/blog/jungian-views-on-aging/)

####  Chrysalis: The Psychology of Transformation

with Marion Woodman Toronto analyst Marion Woodman explores the body/spirit relationship, the withdrawing of projection, gender issues, and the surrender of the ego to the Self as these themes relate…

[Read More](https://jungchicago.org/blog/jungianthology-1-woodman-chrysalis/)